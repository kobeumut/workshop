# Workshop

In first screen, email and password are checked by filtering the entire user list for user login and register. 
In second screen, listing 12 user and have infinite scroll. 
And in third screen, when clicked user in recyclerview, send data to profile screen. And you can edit profile only some field. If you click ok button the user infos save in sharedpreference and update on server.

## Using Features

 - Mvvm
 - LiveData
 - RxJava
 - Value Animator
 - Koin (Dependency Injection)
 - Kotlin Extensions
 - Retrofit
 - Picasso
 - Anko
 - Simple Unit & Instrumental Test

## References
- Network unit test is inspire from source in retrofit repo.
- Login error check is inspire [Tutsplus](https://code.tutsplus.com/tutorials/kotlin-reactive-programming-for-an-android-sign-up-screen--cms-31585)
- Json service on [Json-Generator](https://reqres.in/)

### Shortcomings
- The unit and instrumental tests are developable
- Sqlite database(with room) or realm can be added if desired.
- Maybe navigation component can be added
