package com.umutbey.userlist.viewmodel

import android.graphics.Color
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.umutbey.userlist.model.User
import com.umutbey.userlist.network.ApiInterface
import com.umutbey.userlist.repository.Repository
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.util.*


class ProfileViewModel : ViewModel(), KoinComponent {
    lateinit var viewObservable:ViewObservable
    private val apiInterface: ApiInterface by inject()
    var repo: Repository = Repository(apiInterface)

    fun setBindingUser(user:User){
        viewObservable = ViewObservable(user)
        viewObservable.notifyFields()
    }
    fun updateUser(user:User): MutableLiveData<Boolean> {
        return repo.updateUser(user)
    }

    inner class ViewObservable(val user: User) : BaseObservable() {
        val name: String
            @Bindable
            get() = "${user.first_name} ${user.last_name}"

        val mail: String
            @Bindable
            get() = "Mail: ${user.email}"

        val job: String
            @Bindable
            get() = "Job: ${user.job}"


        val backgroundColor: Int
            @Bindable
            get() = user.fav_color!!

        internal fun notifyFields() {
            notifyPropertyChanged(BR._all)
        }
    }

}
