package com.umutbey.userlist.network

import com.umutbey.userlist.model.User
import com.umutbey.userlist.model.UserLoginResponse
import com.umutbey.userlist.model.UserResponse
import io.reactivex.Flowable
import io.reactivex.Maybe
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface ApiInterface {
    @POST("api/login")
    fun getLogin(@Body user:User): Call<UserLoginResponse>

    @GET("api/users")
    fun getUserList(@Query("page") page:Int): Call<UserResponse>

    @PUT("api/users/{id}")
    fun updateUser(@Path("id") id: Int, @Body user:User): Call<User>

    @POST("api/register")
    fun registerUser(@Body user:HashMap<String,String>): Call<UserLoginResponse>

}
