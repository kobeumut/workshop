package com.umutbey.userlist.repository


import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.umutbey.userlist.helpers.enqueue
import com.umutbey.userlist.model.User
import com.umutbey.userlist.model.UserResponse
import com.umutbey.userlist.network.ApiInterface


class Repository(private val apiInterfaceInterface: ApiInterface) {
    fun getUsers(page:Int): MutableLiveData<UserResponse>? {
        val userListForReturn = MutableLiveData<UserResponse>()
        val call = apiInterfaceInterface.getUserList(page)
        call.enqueue { call, throwable, response ->
            if (response?.body() != null) {
                userListForReturn.value = response.body()
            } else {
                Log.e("error", throwable?.message ?: "response null")
            }
        }
        return userListForReturn
    }

    fun getLogin(email: String, password: String): MutableLiveData<Boolean> {
        val isRealUser = MutableLiveData<Boolean>()
        val user = User(email = email, password = password)
        val call = apiInterfaceInterface.getLogin(user)
        call.enqueue { _, throwable, response ->
            val body = response?.body()
            if (body?.token != null) {
                isRealUser.value = true
            } else {
                Log.e("error", throwable?.message ?: body?.error?:"Network Error")
                isRealUser.value = false
            }
        }
        return isRealUser
    }
    fun updateUser(user: User): MutableLiveData<Boolean> {
        val isRealUser = MutableLiveData<Boolean>()
        val call = user.id?.let { apiInterfaceInterface.updateUser(it,user) }
        call?.enqueue { _, throwable, response ->
            val body = response?.body()
            if (body?.name == user.name) {
                isRealUser.value = true
            } else {
                Log.e("error", throwable?.message ?: "Network Error")
                isRealUser.value = false
            }
        }
        return isRealUser
    }
    fun registerUser(email: String, password: String): MutableLiveData<Boolean> {
        val returnUser = MutableLiveData<Boolean>()
        val user = hashMapOf("email" to email, "password" to password)
        val call = apiInterfaceInterface.registerUser(user)
        call.enqueue { _, throwable, response ->
            val body = response?.body()
            if (body?.token != null && response.code() == 200) {
                returnUser.value = true
            } else {
                Log.e("error", throwable?.message ?: body?.error?:"Network Error")
                returnUser.value = false
            }
        }
        return returnUser
    }
}
