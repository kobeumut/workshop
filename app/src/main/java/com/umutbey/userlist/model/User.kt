package com.umutbey.userlist.model

import android.graphics.Color
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    val id: Int? = null,
    val avatar: String? = null,
    var first_name: String? = null,
    var last_name: String? = null,
    var job: String? = "Unemployed", //This property for update model
    var name: String? = null, //This property for update model
    val email: String? = null,
    val password: String? = null,
    var fav_color: Int? = null
) : Parcelable {

    init {
        val colors = intArrayOf(
            Color.BLUE,
            Color.RED,
            Color.GREEN,
            Color.DKGRAY,
            Color.YELLOW,
            Color.DKGRAY,
            Color.CYAN,
            Color.MAGENTA
        )
        fav_color = colors.random()
    }
}