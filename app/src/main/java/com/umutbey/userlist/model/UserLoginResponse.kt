package com.umutbey.userlist.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserLoginResponse(
    val error: String?=null,
    val token: String?=null
) : Parcelable