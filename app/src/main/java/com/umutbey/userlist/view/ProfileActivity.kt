package com.umutbey.userlist.view

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.view.animation.AccelerateInterpolator
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.umutbey.userlist.ImageModule
import com.umutbey.userlist.R
import com.umutbey.userlist.databinding.ActivityProfileBinding
import com.umutbey.userlist.helpers.cleanup
import com.umutbey.userlist.helpers.put
import com.umutbey.userlist.model.User
import com.umutbey.userlist.viewmodel.ProfileViewModel
import kotlinx.android.synthetic.main.activity_profile.*
import org.jetbrains.anko.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class ProfileActivity : AppCompatActivity() {
    private val viewModel: ProfileViewModel by viewModel()
    private lateinit var binding: ActivityProfileBinding

    private val pref: SharedPreferences by lazy {
        applicationContext.getSharedPreferences(
            MainActivity.PREFS_FILENAME,
            0
        )
    }
    private val imageModule: ImageModule by inject { parametersOf(this@ProfileActivity) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile)
        val user = intent.getParcelableExtra<User>("DATA")
        viewModel.setBindingUser(user)
        binding.viewModel = viewModel
        user.avatar?.let { imageModule.get(it, profile_img, isCircle = true) }
        ValueAnimator.ofInt(Color.TRANSPARENT, viewModel.viewObservable.backgroundColor)
            .apply {
                setEvaluator(ArgbEvaluator())
                addUpdateListener {
                    val value = it.animatedValue as Int
                    binding.backgroundView.setBackgroundColor(value)
                    interpolator = AccelerateInterpolator(1.5f)
                    duration = 1500
                }
            }.start()
        binding.edit.setOnClickListener {
            alert {
                customView {
                    verticalLayout {
                        padding = dip(16)
                        val name = editText(binding.profileName.text) {
                            hint = "Name"
                        }
                        val job = editText(binding.job.text.toString().cleanup()) {
                            hint = "Job"
                        }
                        okButton {
                            val localName = name.text.toString().cleanup()
                            user.name = localName
                            user.job = job.text.toString().cleanup()

                            user.first_name = localName
                            user.last_name = ""
                            viewModel.setBindingUser(user)
                            binding.viewModel = viewModel
                            pref.put(user.id.toString(), user)
                            viewModel.repo.updateUser(user).observeForever {
                                if (it) {
                                    Toast.makeText(this@ProfileActivity, "Update Successfully", Toast.LENGTH_LONG).show()
                                } else {
                                    Toast.makeText(
                                        this@ProfileActivity,
                                        "There is a problem with network",
                                        Toast.LENGTH_LONG
                                    ).show()

                                }
                            }
                        }

                    }
                }
            }.show()
        }

    }
}



